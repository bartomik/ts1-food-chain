package FoodChain.foodStuff;

/**
 * Used to represent various types of food
 */
public enum FoodType {
    GRAPE, MEAT, TOMATO, POTATO, WHEAT
}
