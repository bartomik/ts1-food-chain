package FoodChain.foodStuff;

import FoodChain.Prototype;

/**
 * Potato class
 */
public class Potato extends FoodStuff implements Prototype {

    /**
     * Creates a new Potato with its predefined initial state
     */
    public Potato() {
        super(new FarmedState());
    }

    private Potato(FoodState initialState) {
        super(initialState);
    }

    @Override
    public FoodType getType() {
        return FoodType.POTATO;
    }

    @Override
    public String getName() {
        return "Potato";
    }

    @Override
    public Potato clone() {
        return new Potato(state);
    }
}
