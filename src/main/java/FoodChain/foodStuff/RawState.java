package FoodChain.foodStuff;

/**
 * Initial state used only for the meat
 */
public class RawState implements FoodState {
    @Override
    public void process(FoodStuff context) {
        context.setState(new CleanedState());
    }

    @Override
    public String toString() {
        return "raw";
    }

    @Override
    public StateType getType() { return StateType.RAW; }
}
