package FoodChain.foodStuff;

import FoodChain.Prototype;

/**
 * Grape class
 */
public class Grape extends FoodStuff implements Prototype {

    /**
     * Creates a new Grape with its predefined initial state
     */
    public Grape() {
        super(new FarmedState());
    }

    private Grape(FoodState initialState) {
        super(initialState);
    }

    @Override
    public FoodType getType() {
        return FoodType.GRAPE;
    }

    @Override
    public String getName() {
        return "Grape";
    }

    @Override
    public Grape clone() {
        return new Grape(state);
    }
}
