package FoodChain.foodStuff;

/**
 * Represents the state of food after receiving certification
 */
public class CertifiedState implements FoodState {
    @Override
    public void process(FoodStuff context) {
        context.setState(new FinalProductState());
    }

    @Override
    public String toString() {
        return "certified";
    }

    @Override
    public StateType getType() { return StateType.CERTIFIED; }
}
