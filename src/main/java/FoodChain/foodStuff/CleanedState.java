package FoodChain.foodStuff;

/**
 * State representing that the food has been cleaned
 */
public class CleanedState implements FoodState {
    @Override
    public void process(FoodStuff context) {

        FoodType type = context.getType();

        switch (type) {
            case GRAPE:
            case POTATO:
            case TOMATO:
            case WHEAT:
            case MEAT:
                context.setState(new PackagedState());
                break;
        }

    }

    @Override
    public String toString() {
        return "cleaned";
    }

    @Override
    public StateType getType() { return StateType.CLEANED; }
}
