package FoodChain.foodStuff;

import FoodChain.Prototype;

/**
 * Wheat class
 */
public class Wheat extends FoodStuff implements Prototype {

    /**
     * Creates a new Wheat with its predefined initial state
     */
    public Wheat() {
        super(new FarmedState());
    }

    private Wheat(FoodState initialState) {
        super(initialState);
    }

    @Override
    public FoodType getType() {
        return FoodType.WHEAT;
    }

    @Override
    public String getName() {
        return "Wheat";
    }

    @Override
    public FoodStuff clone() {
        return new Wheat(state);
    }
}
