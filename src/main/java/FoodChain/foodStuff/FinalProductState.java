package FoodChain.foodStuff;

/**
 * Represents the final state of food stuff
 */
public class FinalProductState  implements FoodState {
    @Override
    public void process(FoodStuff foodStuff) {

    }

    @Override
    public String toString() {
        return "final product";
    }

    @Override
    public StateType getType() { return StateType.FINAL; }
}
