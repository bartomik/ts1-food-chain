package FoodChain.foodStuff;

/**
 * Represents the state of food after being packaged
 */
public class PackagedState implements FoodState {
    @Override
    public void process(FoodStuff context) {

        FoodType type = context.getType();

        switch (type) {
            case GRAPE:
            case POTATO:
            case TOMATO:
            case WHEAT:
            case MEAT:
                context.setState(new CertifiedState());
                break;
        }

    }

    @Override
    public String toString() {
        return "packaged";
    }

    @Override
    public StateType getType() { return StateType.PACKAGED; }
}
