package FoodChain.foodStuff;

/**
 * An interface providing the basic methods a food state must implement
 */
public interface FoodState {
    /**
     * Used to process the food stuff and
     * @param context food stuff being processed
     */
    void process(FoodStuff context);

    /**
     * Returns the type of the state
     * @return state's type
     */
    StateType getType();

    /**
     * Returns a string containing description of the state
     * @return string containing description of the state
     */
    String toString();
}
