package FoodChain.foodStuff;

import FoodChain.Prototype;

/**
 * Base class used for representing different types of food
 */
public abstract class FoodStuff implements Prototype {

    protected FoodState state;

    /**
     * Create new food stuff
     * @param initialState the initial state of the food stuff
     */
    public FoodStuff(FoodState initialState) {
        state = initialState;
    }

    protected void setState(FoodState state) {
        this.state = state;
    }

    /**
     * Performs the transition of food to the next state
     * @return the food stuff
     */
    public FoodStuff process() {
        state.process(this);
        return this;
    }

    /**
     * Returns the current food state
     * @return food state
     */
    public FoodState getFoodState() { return state; }

    /**
     * Returns the clone of the food stuff
     * @return food stuff clone
     */
    public abstract FoodStuff clone();

    /**
     * Returns the name of the food
     * @return string containing the name of the food
     */
    public abstract String getName();

    /**
     * Returns the type of the food
     * @return type of the food
     */
    public abstract FoodType getType();

    /**
     * Returns the string containing the name of the food and the state in which it is
     * @return string containing the name of the food and the state in which it is
     */
    public String toString() {
        return getName() + " in " + state.toString() + " state";
    }

}
