package FoodChain.foodStuff;

/**
 * Used to represents various state types
 */
public enum StateType {
    FARMED, CLEANED, CERTIFIED, PACKAGED, FINAL, RAW;
}
