package FoodChain.channel;

import FoodChain.RequestType;
import FoodChain.foodStuff.FoodStuff;
import FoodChain.foodStuff.FoodType;

/**
 * Representation of a channel dedicated to grape requests
 */
public class GrapeChannel extends Channel {
    protected GrapeChannel(RequestType type) {
        super();
        this.acceptedFoodType = FoodType.GRAPE;
        this.acceptedRequestType = type;
    }
}
