package FoodChain.channel;

import FoodChain.RequestType;
import FoodChain.foodStuff.FoodStuff;
import FoodChain.foodStuff.FoodType;

/**
 * Representation of a channel dedicated to meat requests
 */
public class MeatChannel extends Channel {
    protected MeatChannel(RequestType type) {
        super();
        this.acceptedFoodType = FoodType.MEAT;
        this.acceptedRequestType = type;
    }
}
