package FoodChain.channel;

import FoodChain.Request;
import FoodChain.RequestType;
import FoodChain.foodStuff.FoodType;

/**
 * Abstract handler implementing chain of responsibility - delegates requests between channels.
 */
public abstract class Handler {
    protected Handler nextHandler;
    protected RequestType acceptedRequestType;
    protected FoodType acceptedFoodType;

    /**
     * Receives a request from an entity.
     * @param req - Request to be handled
     */
    public void receiveRequest(Request req) {
        if(req.getType().equals(acceptedRequestType) && req.getFoodType().equals(acceptedFoodType)) {
            handleRequest(req);
        } else {
            nextHandler.receiveRequest(req);
        }
    }

    abstract void handleRequest(Request req);
}
