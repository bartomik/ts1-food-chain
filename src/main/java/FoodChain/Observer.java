package FoodChain;

/**
 * Interface for observer classes providing methods for communication with observable objects.
 *
 */
public interface Observer {
    /**
     * Allows observable to notify observer about changes, processes changes.
     * @param observable - Changed observable object.
     */
    void update(Observable observable);
}
