package FoodChain.operations;

import FoodChain.entity.Entity;
import FoodChain.foodStuff.FoodStuff;

/**
 * Represents the transfer operation
 */
public class TransferOperation extends Operation {

    private int price;

    /**
     * TransferOperation constructor
     * @param from operation performer
     * @param to entity that owns the product after operation
     * @param foodStuff product on which the operation has been performed
     * @param price price of the product
     */
    public TransferOperation(Entity from, Entity to, FoodStuff foodStuff, int price) {
        super(from, to, foodStuff);
        this.price = price;
    }

    @Override
    public String toString() {
        return "Transfered from " + from.toString() + " to " + to.toString();
    }

    public int getPrice() {
        return this.price;
    }
}
