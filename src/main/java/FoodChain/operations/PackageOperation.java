package FoodChain.operations;

import FoodChain.entity.Entity;
import FoodChain.foodStuff.FoodStuff;

/**
 * Represents the package operation
 */
public class PackageOperation extends Operation {

    /**
     * PackageOperation constructor
     * @param from operation performer
     * @param to entity that owns the product after operation
     * @param foodStuff product on which the operation has been performed
     */
    public PackageOperation(Entity from, Entity to, FoodStuff foodStuff) {
        super(from, to, foodStuff);
    }

    @Override
    public String toString() {
        return "Packaged by " + from.toString();
    }
}
