package FoodChain.operations;

import FoodChain.entity.Entity;
import FoodChain.foodStuff.FoodStuff;

/**
 * Represents the certification operation
 */
public class CertifyOperation extends Operation {

    /**
     * CertifyOperation constructor
     * @param from operation performer
     * @param to entity that owns the product after operation
     * @param foodStuff product on which the operation has been performed
     */
    public CertifyOperation(Entity from, Entity to, FoodStuff foodStuff) {
        super(from, to, foodStuff);
    }

    @Override
    public String toString() {
        return "Certificated by " + from.toString();
    }
}
