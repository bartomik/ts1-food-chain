package FoodChain.operations;

import FoodChain.entity.Entity;
import FoodChain.foodStuff.FoodStuff;

/**
 * Abstract class used as the basic template for specific operation classes
 */
public abstract class Operation {

    Entity from;
    Entity to;
    FoodStuff foodStuff;

    /**
     * Operation constructor
     * @param from operation perfomer
     * @param to owner of the product after operation completion
     * @param foodStuff product on which the operation has been performed
     */
    public Operation(Entity from, Entity to, FoodStuff foodStuff) {
        this.from = from;
        this.to = to;
        this.foodStuff = foodStuff;
    }

    /**
     * Returns the entity that performed the operation
     * @return operation performer
     */
    public Entity getFrom() {
        return from;
    }

    /**
     * Return the entity that receives the ownership of the product after operation completion
     * @return entity that owns the product after operation
     */
    public Entity getTo() {
        return to;
    }

    /**
     * Returns the food stuff on which the operation has been performed
     * @return food stuff on which the operation has been performed
     */
    public FoodStuff getFoodStuff() {
        return foodStuff;
    }

    /**
     * Returns the information about the operation in a readable format
     * @return string containing the information about the information
     */
    public abstract String toString();

}
