package FoodChain.operations;

import FoodChain.entity.Entity;
import FoodChain.foodStuff.FoodStuff;

/**
 * Represents the clean operation
 */
public class CleanOperation extends Operation {

    /**
     * CleanOperation constructor
     * @param from operation performer
     * @param to entity that owns the product after operation
     * @param foodStuff product on which the operation has been performed
     */
    public CleanOperation(Entity from, Entity to, FoodStuff foodStuff) {
        super(from, to, foodStuff);
    }

    @Override
    public String toString() {
        return "Cleaned by " + from.toString();
    }
}
