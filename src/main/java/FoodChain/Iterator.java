package FoodChain;

/**
 * Iterator interface.
 * @param <E> - Class to iterate over.
 */
public interface Iterator<E> extends java.util.Iterator<E> {
    boolean hasNext();
    E next();
}
