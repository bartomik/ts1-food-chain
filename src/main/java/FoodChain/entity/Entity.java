package FoodChain.entity;

import FoodChain.Observable;
import FoodChain.Observer;
import FoodChain.Request;
import FoodChain.blockchain.Block;
import FoodChain.blockchain.BlockchainClient;
import FoodChain.blockchain.MoneyTransfer;
import FoodChain.channel.Channel;
import FoodChain.foodStuff.FoodStuff;
import FoodChain.foodStuff.FoodType;
import FoodChain.operations.Operation;
import FoodChain.operations.TransferOperation;

import java.security.*;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Representation of an entity in food chain cycle.
 */
public class Entity implements Observer {

    protected List<Request> offers;
    protected List<FoodStuff> processing;
    protected List<Request> toDoList;
    protected Channel firstChannel;

    protected BlockchainClient blockchainClient;
    protected Strategy strategy;
    private PrivateKey privateKey;
    private PublicKey publicKey;

    /**
     * Creates entity instance with given keys.
     * @param firstChannel first channel used to communicate offers and requests
     * @param privateKey RSA private key
     * @param publicKey RSA public key
     */
    public Entity(Channel firstChannel, PrivateKey privateKey, PublicKey publicKey) {
        this.firstChannel = firstChannel;
        this.processing = new ArrayList<>();
        this.toDoList = new ArrayList<>();
        this.offers = new ArrayList<>();
        this.privateKey = privateKey;
        this.publicKey = publicKey;
        this.blockchainClient = new BlockchainClient(this, privateKey, publicKey);
    }

    /**
     * Creates entity instance with given money balance and strategy.
     * @param firstChannel first channel used to communicate offers and requests
     * @param moneyBalance initial amount of money owned by the entity
     * @param strategy initial strategy of the entity
     */
    public Entity(Channel firstChannel, double moneyBalance, Strategy strategy) {

        this.firstChannel = firstChannel;
        processing = new ArrayList<>();
        toDoList = new ArrayList<>();
        this.strategy = strategy;
        this.offers = new ArrayList<>();

        // Create the key pair
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            KeyPair pair = keyPairGenerator.generateKeyPair();
            privateKey = pair.getPrivate();
            publicKey = pair.getPublic();
        } catch (NoSuchAlgorithmException exception) {
            System.out.println(exception.getMessage());
        }

        blockchainClient = new BlockchainClient(this, privateKey, publicKey, moneyBalance);
    }

    /**
     * Gets entity's public key.
     * @return Public key of entity
     */
    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void receiveMoney(MoneyTransfer transfer) {
        blockchainClient.getWallet().receiveMoney(transfer.getAmount());
    }

    /**
     * Method implementing the observer interface
     */
    @Override
    public void update(Observable observable) {
        strategy.update(this, observable);
    }

    public BlockchainClient getBlockchainClient() {
        return blockchainClient;
    }

    /**
     * A method being called from the simulation, upon which the entity interacts
     * with its environment according to its current strategy. A timestamp is provided
     * so that all executed actions can be correctly marked using this timestamp.
     * @param currentTimestamp current step of the simulation
     */
    public void act(int currentTimestamp) {
        strategy.moneySpentInSimulationStep = 0;
        blockchainClient.cleanPerformedOperations();
        sendOfferedFoodstuff(currentTimestamp);
        strategy.execute(this, currentTimestamp);
    }

    private void sendOfferedFoodstuff(int currentTimeStamp) {
        if (offers.isEmpty()) return;

        for(Request request : offers) {
            if(request.isAccepted()) {
                List<Map.Entry<FoodStuff, Block>> list = getBlockchainClient().getWallet().getAllFoodOfType(request.getFoodType());
                for(Map.Entry<FoodStuff, Block> entry : list) {
                    if(entry.getKey().getFoodState().getType() == request.getStateType()) {
                        Operation op = new TransferOperation(this, request.getTo(), entry.getKey(), request.getPrice());
                        getBlockchainClient().storeOperation(op, entry.getValue(), currentTimeStamp);
                        request.setDone();
                        break;
                    }
                }
            }
        }

        offers = offers.stream().filter(Predicate.not(Request::isDone)).collect(Collectors.toList());
    }

    /**
     * Changes the strategy utilized by the entity during its turn
     * @param strategy an object extending the Strategy class
     */
    protected void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public String generateTransactionsReport() {
        StringBuilder report = new StringBuilder();
        report.append("Operations performed by ").append(this.toString()).append(":\n");
        if (this.blockchainClient.getPerformedOperations().isEmpty()) {
            report.append("- None");
        } else {
            for (Operation operation : this.blockchainClient.getPerformedOperations()) {
                report.append("- ").append(operation.toString()).append("\n");
            }
        }

        return report.toString();
    }

    /**
     * Generates a report of entity's possessions.
     * @return report of entity's possessions.
     */
    public Map.Entry<Double, Map<FoodType, Integer>> generateReport() {
        Double moneyBalance = blockchainClient.getWallet().getMoneyBalance();
        Map<FoodType, Integer> ownedFood = blockchainClient.getWallet().getAvailableFoodsCount();
        return new AbstractMap.SimpleEntry<>(moneyBalance, ownedFood);
    }
}
