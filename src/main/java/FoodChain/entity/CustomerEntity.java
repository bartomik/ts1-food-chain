package FoodChain.entity;

import FoodChain.channel.Channel;

/**
 * Representation of a customer.
 */
public class CustomerEntity extends Entity {
    protected CustomerEntity(Channel firstChannel, double money, Strategy strategy) {
        super(firstChannel, money, strategy);
    }
}
