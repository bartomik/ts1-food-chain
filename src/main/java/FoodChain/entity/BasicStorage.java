package FoodChain.entity;

import FoodChain.Observable;
import FoodChain.blockchain.Block;
import FoodChain.channel.Channel;
import FoodChain.foodStuff.FoodStuff;
import FoodChain.foodStuff.StateType;
import FoodChain.operations.CleanOperation;
import FoodChain.operations.Operation;
import FoodChain.operations.PackageOperation;

import java.util.Map;

/**
 * Basic strategy for storages - buy cleaned foodstuff, process and sell packaged foodstuff in highest numbers possible.
 */
public class BasicStorage extends Strategy {
    protected BasicStorage() {
        this.toBuy = StateType.CLEANED;
        this.toSell = StateType.PACKAGED;
    }

    /**
     * Automatizes storage actions - fulfill requests, process and demand more foodstuff.
     * @param context - Entity using strategy
     * @param currentTimestamp - Step in simulation
     */
    @Override
    public void execute(Entity context, int currentTimestamp) {
        sendOutRequestedFoodStuff(context, currentTimestamp);
        processOrOfferStock(context, currentTimestamp);
        demandFoodStuff(context, 3);
    }

    /**
     * Accepts requests according to strategy settings.
     * @param context - Entity using strategy
     * @param channel - Observable channel object
     */
    @Override
    public void update(Entity context, Observable channel) {
        takeRequests(context, ((Channel)channel).getLastRequest());
    }

    /**
     * Packs given foodstuff.
     * @param context - Entity using strategy
     * @param foodStuff - Foodstuff to process
     * @param currentTimestamp - Step in simulation
     */
    @Override
    public void processFoodStuff(Entity context, Map.Entry<FoodStuff, Block> foodStuff, int currentTimestamp) {
        FoodStuff processed = foodStuff.getKey().process();
        Operation operation = new PackageOperation(context, context, processed);
        context.getBlockchainClient().storeOperation(operation, foodStuff.getValue(), currentTimestamp);
    }
}
