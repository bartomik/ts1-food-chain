package FoodChain.entity;

import FoodChain.Observable;
import FoodChain.blockchain.Block;
import FoodChain.foodStuff.FoodStuff;
import FoodChain.foodStuff.FoodType;
import FoodChain.foodStuff.Meat;
import FoodChain.foodStuff.StateType;
import FoodChain.operations.FarmOperation;
import FoodChain.operations.GatherOperation;
import FoodChain.operations.Operation;

import java.util.Map;

/**
 * Strategy for farmer focusing on meat production.
 */
public class ProductiveMeatFarmer extends Strategy {
    protected ProductiveMeatFarmer(){

    }

    /**
     * Automatizes meat production, switches to hustle strategy if stock high enough.
     * @param context - Entity using strategy
     * @param currentTimestamp - Step in simulation
     */
    @Override
    public void execute(Entity context, int currentTimestamp) {
        FoodStuff food = new Meat();

        Operation farm = new GatherOperation(context, context, food);
        context.getBlockchainClient().storeOperation(farm, null, currentTimestamp);

        if(context.getBlockchainClient().getWallet().getAllFoodOfType(FoodType.MEAT).size() > 3) {
            context.setStrategy(new HustleFarmer(StateType.RAW));
        }
    }


    /**
     * Does nothing - takes no requests as focus is solely on production.
     * @param context - Entity using strategy
     * @param channel - Observable channel object
     */
    @Override
    public void update(Entity context, Observable channel) {

    }

    /**
     * Does nothing - farmer only generates or sells raw/farmed foodstuff - no processing required from him.
     * @param context - Entity processing the food
     * @param foodStuff - Food stuff to process
     * @param currentTimestamp - simulation's timestamp
     */
    @Override
    public void processFoodStuff(Entity context, Map.Entry<FoodStuff, Block> foodStuff, int currentTimestamp) {

    }
}
