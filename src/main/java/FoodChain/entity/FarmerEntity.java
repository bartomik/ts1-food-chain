package FoodChain.entity;

import FoodChain.channel.Channel;

/**
 * Representation of a farmer.
 */
public class FarmerEntity extends Entity {

    protected FarmerEntity(Channel firstChannel, double money, Strategy strategy) {
        super(firstChannel, money, strategy);
    }
}
