package FoodChain.entity;

import FoodChain.channel.Channel;
import FoodChain.foodStuff.StateType;

/**
 * Simple factory for entities, sets default strategies usually.
 */
public class EntityFactory {
    private Channel firstChannel;

    /**
     * Creates an entity factory.
     * @param firstChannel - Channel responsible for accepting requests
     */
    public EntityFactory(Channel firstChannel) {
        this.firstChannel = firstChannel;
    }

    /**
     * Creates a farmer starting off with producing vegetables.
     * @param money - Initial balance
     * @return Farmer entity
     */
    public FarmerEntity makeVegFarmer(double money) {
        return new FarmerEntity(firstChannel, money, new ProductiveVegFarmer());
    }

    /**
     * Creates a farmer starting off with producing meat.
     * @param money - Initial balance
     * @return Farmer entity
     */
    public FarmerEntity makeMeatFarmer(double money) {
        return new FarmerEntity(firstChannel, money, new ProductiveMeatFarmer());
    }

    /**
     * Creates a processor with default strategy.
     * @param money - Initial balance
     * @param toBuy - state type desired by the processor
     * @return Processor entity
     */
    public ProcessorEntity makeProcessor(double money, StateType toBuy) {
        return new ProcessorEntity(firstChannel, money, new BasicProcessor(toBuy));
    }

    /**
     * Creates a storage with default strategy.
     * @param money - Initial balance
     * @return Storage entity
     */
    public StorageEntity makeStorage(double money) {
        return new StorageEntity(firstChannel, money, new BasicStorage());
    }

    /**
     * Creates a distributor with default strategy.
     * @param money - Initial balance
     * @return Distributor entity
     */
    public DistributorEntity makeDistributor(double money) {
        return new DistributorEntity(firstChannel, money, new BasicDistributor());
    }

    /**
     * Creates a seller with default strategy.
     * @param money - Initial balance
     * @return Seller entity
     */
    public SellerEntity makeSeller(double money) {
        return new SellerEntity(firstChannel, money, new BasicSeller());
    }

    /**
     * Creates a customer with default strategy.
     * @param money - Initial balance
     * @return Customer entity
     */
    public CustomerEntity makeCustomer(double money) {
        return new CustomerEntity(firstChannel, money, new BasicCustomer());
    }


}
