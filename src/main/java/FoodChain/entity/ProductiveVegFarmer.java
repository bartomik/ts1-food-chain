package FoodChain.entity;


import FoodChain.Observable;
import FoodChain.RequestType;
import FoodChain.blockchain.Block;
import FoodChain.channel.Channel;
import FoodChain.foodStuff.*;
import FoodChain.operations.FarmOperation;
import FoodChain.operations.Operation;

import java.util.Map;
import java.util.Random;

/**
 * Strategy for farmer focusing on meat production.
 */
public class ProductiveVegFarmer extends Strategy {

    public ProductiveVegFarmer() {
    }

    /**
     * Automatizes production of random vegetables, switches to hustle strategy if stock high enough.
     * @param context - Entity using strategy
     * @param currentTimestamp - Step in simulation
     */

    @Override
    public void execute(Entity context, int currentTimestamp) {
        FoodStuff food;

        //could be changed to generate random food type from enum class - foodstuff factory needed
        Random random = new Random();
        FoodType farmed;
        int ran = random.nextInt(4);

        switch(ran) {
            case 0:
                farmed = FoodType.WHEAT;
                food = new Wheat();
                break;
            case 1:
                farmed = FoodType.GRAPE;
                food = new Grape();
                break;
            case 2:
                farmed = FoodType.TOMATO;
                food = new Tomato();
                break;
            case 3:
                farmed = FoodType.POTATO;
                food = new Potato();
                break;
            default:
                farmed = FoodType.WHEAT;
                food = new Wheat();
                break;
        }

        Operation farm = new FarmOperation(context, context, food);
        context.getBlockchainClient().storeOperation(farm, null, currentTimestamp);

        if(context.getBlockchainClient().getWallet().getAllFoodOfType(farmed).size() > 2) {
            context.setStrategy(new HustleFarmer(StateType.FARMED));
        }
    }

    /**
     * Does nothing - takes no requests as focus is solely on production.
     * @param context - Entity using strategy
     * @param channel - Observable channel object
     */
    @Override
    public void update(Entity context, Observable channel) {
    }

    /**
     * Does nothing - farmer only generates or sells raw/farmed foodstuff - no processing required from him.
     * @param context - Entity using strategy
     * @param foodStuff - Foodstuff to process
     * @param currentTimestamp - Step in simulation
     */
    @Override
    public void processFoodStuff(Entity context, Map.Entry<FoodStuff, Block> foodStuff, int currentTimestamp) {

    }
}
