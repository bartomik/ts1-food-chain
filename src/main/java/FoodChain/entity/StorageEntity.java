package FoodChain.entity;

import FoodChain.channel.Channel;

/**
 * Representation of a storage.
 */
public class StorageEntity extends Entity {
    protected StorageEntity(Channel firstChannel, double money, Strategy strategy) {
        super(firstChannel, money, strategy);
    }
}
