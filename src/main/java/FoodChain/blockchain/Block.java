package FoodChain.blockchain;

import FoodChain.Iterable;
import FoodChain.Iterator;
import FoodChain.Prototype;
import FoodChain.operations.Operation;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.*;

/**
 * Elementary unit of the blockchain, contains informations about performed operations
 */
public class Block implements Prototype, Iterable<Block> {

    private Block previousBlock;
    private String previousBlockHash;
    private Block inputBlock;
    private int timestamp;
    private Operation operation;
    private byte[] signature;

    /**
     * Create a new block with the passed configuration
     * @param previousBlock previous block contained in the blockchain
     * @param previousBlockHash hash of the previous block at the time of the construction of the current block
     * @param inputBlock block containing information about the previous operation with regards to the contained food stuff
     * @param operation performed operation information
     * @param timestamp the simulation timestamp at the time of construction
     */
    public Block(Block previousBlock, String previousBlockHash, Block inputBlock, Operation operation, int timestamp) {
        this.previousBlock = previousBlock;
        this.previousBlockHash = previousBlockHash;
        this.inputBlock = inputBlock;
        this.operation = operation;
        this.timestamp = timestamp;
    }

    /**
     * Used for testing purposes only
     * @param previousBlock previous block contained in the blockchain
     * @param previousBlockHash hash of the previous block at the time of the construction of the current block
     * @param inputBlock block containing information about the previous operation with regards to the contained food stuff
     * @param operation performed operation information
     * @param timestamp the simulation timestamp at the time of construction
     * @param signature the signature assigned to the block
     */
    public Block(Block previousBlock, String previousBlockHash, Block inputBlock, Operation operation, int timestamp, byte[] signature) {
        this.previousBlock = previousBlock;
        this.previousBlockHash = previousBlockHash;
        this.inputBlock = inputBlock;
        this.operation = operation;
        this.timestamp = timestamp;
        this.signature = signature;
    }

    /**
     * Returns the hash of the block
     * @return String containing the hash
     */
    public String getHash() {

        StringBuilder infoBuilder = new StringBuilder();
        if (previousBlock != null) {
            infoBuilder.append(previousBlock.getHash());
        }

        infoBuilder.append(operation.toString());
        infoBuilder.append(timestamp);

        if (signature != null) {
            infoBuilder.append(new String(signature, StandardCharsets.UTF_8));
        }

        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
            return null;
        }

        byte[] hashBytes = messageDigest.digest(infoBuilder.toString().getBytes());
        BigInteger number = new BigInteger(1, hashBytes);

        StringBuilder hash = new StringBuilder(number.toString(16));
        while (hash.length() < 32) {
            hash.insert(0, "0");
        }

        return hash.toString();

    }

    /**
     * Constructs the message that is being signed by the entity that created the block
     * @return String containing the message
     */
    public String getMessageToSign() {
        StringBuilder toSign = new StringBuilder();

        if (operation != null) {
            toSign.append(operation.toString());
        }

        if (previousBlockHash != null) {
            toSign.append(previousBlockHash);
        }

        if (inputBlock != null) {
            inputBlock.getHash();
        }

        toSign.append(timestamp);

        return toSign.toString();
    }

    /**
     * Signs the key with a private key so that its authenticity can be verified
     * @param key private key to sign the block with
     */
    public void sign(PrivateKey key) throws InvalidSignature {

        if (signature != null) return;

        String toSign = getMessageToSign();

        try {
            Cipher encrypt = Cipher.getInstance("RSA");
            encrypt.init(Cipher.ENCRYPT_MODE, key);
            signature = encrypt.doFinal(toSign.getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException exception) {
            throw new InvalidSignature();
    //            System.out.println(exception.getMessage());
        }
    }

    /**
     * Verifies that the owner of the provided public key signed the block with his private key
     * @param key public key
     * @return boolean specifying whether the block is authentic or not
     * @throws BadPaddingException is thrown when incorrect public key is used
     */
    public boolean verifySignature(PublicKey key) throws BadPaddingException {

        if (signature == null) return false;

        String decryptedSignature = null;
        try {
            Cipher decrypt = Cipher.getInstance("RSA");
            decrypt.init(Cipher.DECRYPT_MODE, key);
            decryptedSignature = new String(decrypt.doFinal(signature), StandardCharsets.UTF_8);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | InvalidKeyException | IllegalBlockSizeException exception) {
            return false;
        }

        String expectedSignature = getMessageToSign();

        return decryptedSignature != null && decryptedSignature.equals(expectedSignature);
    }

    /**
     * Generates report that includes everything that has happened to the contained FoodStuff to date
     * @return String containing the report
     */
    public String generateReport() {

        StringBuilder report = new StringBuilder();

        Iterator<Block> it = iterator();
        Block currentBlock;
        while (it.hasNext()) {
            currentBlock = it.next();
            report.insert(0, currentBlock.operation.toString() + "\n");
        }

        report.insert(0, operation.getFoodStuff().getName() + "\n");

        return report.toString();

    }

    /**
     * Creates clone of the block
     * @return block's clone
     */
    @Override
    public Prototype clone() {

        return new Block(
                previousBlock,
                previousBlockHash,
                inputBlock,
                operation,
                timestamp,
                signature == null ? null : signature.clone()
        );
    }

    /**
     * Provides an iterator over blocks that contain operations with regard to the same food
     * @return an iterator
     */
    @Override
    public Iterator<Block> iterator() {
        return new BlockIterator(this);
    }

    protected Block getPreviousBlock() {
        return previousBlock;
    }

    protected String getPreviousBlockHash() {
        return previousBlockHash;
    }

    /**
     * Returns the input block, which points to the previous block that contains information
     * about previously performed operation on the contained food
     * @return previous Block with respect to the contained food
     */
    public Block getInputBlock() {
        return inputBlock;
    }

    /**
     * Return the information about the contained performed operation
     * @return Operation object
     */
    public Operation getOperation() { return operation; }

    protected int getTimestamp() {
        return timestamp;
    }

    protected byte[] getSignature() {
        return signature.clone();
    }

    // FOR TESTING PURPOSES ONLY
    public void setSignature(byte[] signature) {
        this.signature = signature;
    };

    // FOR TESTING PURPOSES ONLY
    public void setOperation(Operation operation) {
        this.operation = operation;
    }

}
