package FoodChain.blockchain;

import FoodChain.entity.Entity;
import FoodChain.foodStuff.FoodStuff;
import FoodChain.foodStuff.FoodType;
import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Contains entity's properties and money
 */
public class  Wallet {

    private Entity owner;

    // Store the food and the input block
    private Map<FoodStuff, Block> availableFood;
    private MonetaryAmount moneyBalance;

    /**
     * Creates a new wallet
     * @param owner entity that owns the wallet
     * @param moneyBalance initial money balance
     */
    public Wallet(Entity owner, double moneyBalance) {
        this.owner = owner;
        this.moneyBalance = Money.of(moneyBalance, "EUR");
        this.availableFood = new HashMap<>();
    }

    /**
     * Creates a new wallet
     * @param owner entity that owns the wallet
     */
    public Wallet(Entity owner) {
        this.owner = owner;
        this.moneyBalance = Money.of(1000, "EUR");
        this.availableFood = new HashMap<>();
    }

    /**
     * Get money balance
     * @return money balance
     */
    public double getMoneyBalance() {
        return moneyBalance.getNumber().doubleValue();
    }

    /**
     * Receives specified amount of money
     * @param amount received amount
     */
    public void receiveMoney(MonetaryAmount amount) {
        moneyBalance = moneyBalance.add(amount);
    }

    /**
     * Method to withdraw a certain amount of money from the wallet.
     * This method is protected so that it can be used to create MoneyTransfers,
     * which in turn guarantee the origin of the money
     * @param amount amount of money to withdraw
     * @return MonetaryAmount
     */
    protected MonetaryAmount withdrawMoney(double amount) {
        moneyBalance = moneyBalance.subtract(Money.of(amount, "EUR"));
        return Money.of(amount, "EUR");
    }

    /**
     * Transfers money from this wallet to the wallet of the receiver
     * @param receiver Entity set to receive the money
     * @param amount The amount of money to be transferred
     */
    public void transferMoneyTo(Entity receiver, double amount) {
        MoneyTransfer transfer = MoneyTransfer.createTransfer(this, amount);
        receiver.receiveMoney(transfer);
    }

    /**
     * Stores the newly acquired food in the waller
     * @param food FoodStuff representing the food
     * @param inputBlock Block according to which the food has been acquired, used to prove the ownership
     */
    public void storeFood(FoodStuff food, Block inputBlock) {
        availableFood.put(food, inputBlock);
    }

    void removeFood(FoodStuff food) { availableFood.remove(food); }

    /**
     * Counts the available food by type
     * @return hash map containing how much food of every type is stored in the wallet
     */
    public Map<FoodType, Integer> getAvailableFoodsCount() {
        Map<FoodType, Integer> counts = new HashMap<>();

        for (FoodStuff foodStuff : availableFood.keySet()) {
            if (counts.containsKey(foodStuff.getType())) {
                counts.put(foodStuff.getType(), counts.get(foodStuff.getType()) + 1);
            } else {
                counts.put(foodStuff.getType(), 1);
            }
        }

        return counts;
    }

    // TODO: It might prove useful to create a method that automatically processes all the available food

    /**
     * Returns all available food of the specified type
     * This method should be used mainly to process all food of the specified type
     * @param type type of food to return
     * @return list containing all available food of the specified type and its input blocks
     */
    public List<Map.Entry<FoodStuff, Block>> getAllFoodOfType(FoodType type) {
        return availableFood.entrySet().stream().filter(entry -> entry.getKey().getType().equals(type)).collect(Collectors.toList());
    }

    /**
     * If the the specified food type is available it is returned
     * This method should be used mainly for selling products
     * @param type required food type
     * @return FoodStuff representing the required food and the input block used to prove the ownership or null
     */
    public Map.Entry<FoodStuff, Block> getFood(FoodType type) {
        Optional<Map.Entry<FoodStuff, Block>> food = availableFood.entrySet().stream().filter(entry -> entry.getKey().getType().equals(type)).findFirst();

        if (food.isEmpty()) return null;

        // Remove the food from the available food
        availableFood.remove(food.get().getKey());

        return food.get();
    }

    /**
     * Returns the owner of this wallet
     * @return Entity object specifying the owner of this wallet
     */
    protected Entity getOwner() {
        return owner;
    }
}
