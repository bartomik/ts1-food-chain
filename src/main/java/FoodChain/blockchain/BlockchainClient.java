package FoodChain.blockchain;

import FoodChain.Observable;
import FoodChain.Observer;
import FoodChain.entity.Entity;
import FoodChain.operations.Operation;
import FoodChain.operations.TransferOperation;

import java.security.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Used for communication with other blockchain users, storing new operations in blockchain and receiving items
 */
public class BlockchainClient implements Observer {

    private Blockchain blockchain;
    private Wallet wallet;
    private PrivateKey privateKey;
    private PublicKey publicKey;
    private List<Operation> performedOperations;

    /**
     * Creates a new blockchain client used for operations on blockchain
     * @param owner entity that owns this client
     * @param ownerPrivateKey entity's private RSA key
     * @param ownerPublicKey entity's public RSA key
     * @param moneyBalance initial amount of money contained in the wallet
     */
    public BlockchainClient(Entity owner, PrivateKey ownerPrivateKey, PublicKey ownerPublicKey, double moneyBalance) {
        blockchain = new Blockchain();
        wallet = new Wallet(owner, moneyBalance);
        privateKey = ownerPrivateKey;
        publicKey = ownerPublicKey;
        performedOperations = new ArrayList<>();
        BlockchainDistributor.getInstance().attachListener(this);
    }

    /**
     * Creates a new blockchain client used for operations on blockchain
     * @param owner entity that owns this client
     * @param ownerPrivateKey entity's private RSA key
     * @param ownerPublicKey entity's public RSA key
     */
    public BlockchainClient(Entity owner, PrivateKey ownerPrivateKey, PublicKey ownerPublicKey) {
        blockchain = new Blockchain();
        wallet = new Wallet(owner);
        privateKey = ownerPrivateKey;
        publicKey = ownerPublicKey;
        performedOperations = new ArrayList<>();
        BlockchainDistributor.getInstance().attachListener(this);
    }

    public Blockchain getBlockchain() {
        return blockchain;
    }

    /**
     * Stores the performed operation in the blockchain
     * @param operation performed
     * @param inputBlock block providing the evidence of
     * @param timestamp the time at which the operation has been performed
     */
    public void storeOperation(Operation operation, Block inputBlock, int timestamp) {

        performedOperations.add(operation);

        Block lastBlock = blockchain.getLastBlock();

        Block newBlock = new Block(
                lastBlock,
                lastBlock == null ? null : lastBlock.getHash(),
                inputBlock,
                operation,
                timestamp
        );

        try {
            newBlock.sign(privateKey);
        } catch(InvalidSignature e) {
            System.out.println(e.getMessage());
        }

        sendTransaction(newBlock);
    }

    /**
     * Sends the created block to the distributor
     * @param block new block that has been appended to the blockchain
     */
    void sendTransaction(Block block) {
        BlockchainDistributor.getInstance().distributeBlock(block);
    }

    /**
     * Returns list of operations that have been performed in the current simulation step
     * by the entity that owns this blockchain client
     * @return list of performed operations
     */
    public List<Operation> getPerformedOperations() {
        return performedOperations;
    }

    /**
     * Used to clean list of performed operations at the beginning of a new simulation step
     */
    public void cleanPerformedOperations() {
        this.performedOperations.clear();
    }

    @Override
    public void update(Observable obs) {
        // Get the new block to be added to the FoodChain.blockchain
        Block newBlock = BlockchainDistributor.getInstance().getNewBlock();

        try {
            blockchain.appendBlock(newBlock);
        } catch (InvalidHashException | DoubleSpendingException | InvalidSignature exception) {
            // The block couldn't be accepted because its previous block hash didn't match the tail's hash or
            // because double spending has been detected
            // System.out.println(exception.getMessage());
            BlockchainDistributor.getInstance().clientResponse(this, false);
            return;
        }

        // Store the foodStuff if the owner of this BlockchainClient is the receiver of the operation's resulting product
        if (newBlock.getOperation().getTo().getPublicKey().equals(publicKey)) {
            wallet.storeFood(newBlock.getOperation().getFoodStuff(), newBlock);

            if(newBlock.getOperation().getClass() == TransferOperation.class) {
                TransferOperation op = (TransferOperation)newBlock.getOperation();
                wallet.transferMoneyTo(op.getFrom(), op.getPrice());
            }
        }

        // The block has been accepted
        BlockchainDistributor.getInstance().clientResponse(this, true);
    }

    /**
     * Returns the wallet, which stores the food owned by the entity as well as the money.
     * Provides the entity with ability to perform actions with the owned food and to send and receive money.
     * @return Wallet
     */
    public Wallet getWallet() {
        return wallet;
    }
}
