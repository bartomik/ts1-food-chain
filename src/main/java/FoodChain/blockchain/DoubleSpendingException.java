package FoodChain.blockchain;

/**
 * An exception thrown when double spending has been detected
 */
public class DoubleSpendingException extends Exception {
    public DoubleSpendingException() { super("EXCEPTION: Double spending"); }
    public DoubleSpendingException(String message) {
        super(message);
    }
}
