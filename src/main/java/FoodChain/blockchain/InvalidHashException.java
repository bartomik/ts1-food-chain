package FoodChain.blockchain;

/**
 * An exception thrown when hash mismatch occured when appending a new block to the blockchain
 */
public class InvalidHashException extends Exception {
    public InvalidHashException() { super("EXCEPTION: Invalid hash"); }
    public InvalidHashException(String message) {
        super(message);
    }
}
