package FoodChain.blockchain;

import FoodChain.Observable;
import FoodChain.Observer;
import FoodChain.entity.Entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Contains the logic for distributing new blocks among the blockchain clients and detects double spending
 */
public final class BlockchainDistributor implements Observable {

    private static BlockchainDistributor instance;
    private List<Observer> listeners;
    private Map<Observer, Boolean> clientResponses;
    private Map<Entity, Integer> doubleSpendingAttempts;
    private Block newBlock;

    private BlockchainDistributor() {
        listeners = new ArrayList<>();
        clientResponses = new HashMap<>();
        doubleSpendingAttempts = new HashMap<>();
    }

    /**
     * Returns an instance of the blockchain distributor
     * @return blockchain distributor instance
     */
    public static BlockchainDistributor getInstance() {
        if (instance == null) {
            instance = new BlockchainDistributor();
        }

        return instance;
    }

    /**
     * Returns the block that should be distributed
     * @return block to distribute
     */
    public Block getNewBlock() {
        return newBlock;
    }

    /**
     * Receives a new block to distribute and notifies all the listeners
     * @param block block to distribute
     */
    public void distributeBlock(Block block) {
        clientResponses.clear();
        newBlock = block;
        notifyListeners();
    }

    /**
     * Receives a response from the client, that specifies whether the distributed block has been
     * accepted by the client or not. After all the clients have responded, performs a check that at least
     * half of the clients accepted the block and that therefore the block is valid
     * @param observer client that received the new block
     * @param accepted confirmation of validity of the block
     */
    public void clientResponse(Observer observer, boolean accepted) {
        clientResponses.put(observer, accepted);

        // Check the overall result after all the clients have responded
        if (clientResponses.keySet().size() == listeners.size()) {
            int acceptedCount = 0;
            for (Boolean response : clientResponses.values()) {
                if (response) acceptedCount++;
            }

            if (acceptedCount <= listeners.size() / 2) {
                handleRejectedBlock();
            }
        }

    }

    private void handleRejectedBlock() {
        // Log the rejected block and log the reason for rejection
        Entity fraudster = newBlock.getOperation().getFrom();
        if (doubleSpendingAttempts.containsKey(fraudster)) {
            doubleSpendingAttempts.put(fraudster, doubleSpendingAttempts.get(fraudster) + 1);
        } else {
            doubleSpendingAttempts.put(fraudster, 1);
        }
    }

    public Map<Entity, Integer> getDoubleSpendingAttempts() {
        return doubleSpendingAttempts;
    }

    /**
     * Generates the security report, that contains information about who attempted to
     * double spend any type of resource and how many times
     * @return String containing the report
     */
    public String generateSecurityReport() {
        StringBuilder report = new StringBuilder();
        report.append("=== Security report ===\n").append("Number of double spending attempts:\n");
        doubleSpendingAttempts.forEach((entity, integer) -> report.append(entity.toString()).append(" ").append(integer.toString()).append("\n"));
        if (doubleSpendingAttempts.isEmpty()) {
            report.append("No double spending attempts occurred");
        }
        return report.toString();
    }

    /**
     * Clears the log of double spending attempts so that the distributor
     * is prepared for a new generation
     */
    public void clearDoubleSpendingAttempts() {
        doubleSpendingAttempts.clear();
    }

    /**
     * Attaches a blockchain client as an observer, who in turn receives information about newly appended blocks
     * @param observer blockchain client to attach
     */
    @Override
    public void attachListener(Observer observer) {
        if (!(observer instanceof BlockchainClient)) return;

        if (!listeners.contains(observer)) {
            listeners.add(observer);
        }
    }

    /**
     * Detaches a blockchain client from the blockchain distributor
     * @param observer blockchain client to detach
     */
    @Override
    public void detachListener(Observer observer) {
        listeners.remove(observer);
    }

    /**
     * Notifies all observers about a new change in the blockchain
     */
    @Override
    public void notifyListeners() {
        listeners.forEach(listener -> listener.update(this));
    }
}
