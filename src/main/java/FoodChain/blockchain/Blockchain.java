package FoodChain.blockchain;

import FoodChain.Iterable;
import FoodChain.Iterator;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * The main class used for representing the blockchain, contains the logic with regards to maintaining the integrity of blocks
 */
public class Blockchain implements Iterable<Block> {

    private Block tail;

    /**
     * Create a new empty blockchain
     */
    public Blockchain() { tail = null; }

    /**
     * Used for testing purposes only
     * @param tail last block in the chain
     */
    public Blockchain(Block tail) {
        this.tail = tail;
    }

    /**
     * Verifies the integrity of the whole blockchain
     * @return boolean representing the correctness of the blockchain
     */
    public boolean verify() {

        // If the FoodChain.blockchain consists only of the genesis block, it is valid
        if (tail.getPreviousBlock() == null) {
            return true;
        }

        // Verify that the signature of the last block is correct
        try {
            if (!tail.verifySignature(tail.getOperation().getFrom().getPublicKey())) {
                return false;
            }
        } catch (BadPaddingException exception) {
            // Incorrect key has been used
            return false;
        }

        // Compute the hash of the previous block and compare it with the stored hash
        String computedPreviousBlockHash = tail.getPreviousBlock().getHash();
        return tail.getPreviousBlockHash().equals(computedPreviousBlockHash);

    }

    /**
     * Returns the last block that has been added to the chain
     * @return last block
     */
    public Block getLastBlock() {
        return tail;
    }

    /**
     * Appends a new block to the blockchain
     * @param block new block
     * @throws InvalidSignature the digital signature of the block is not valid
     * @throws InvalidHashException the hash of the local last block and the hash stored in the new block do not match
     * @throws DoubleSpendingException double spending has been detected
     */
    public void appendBlock(Block block) throws InvalidSignature, InvalidHashException, DoubleSpendingException {

        // Verify the block signature
        try {
            if (!block.verifySignature(block.getOperation().getFrom().getPublicKey())) {
                throw new InvalidSignature();
            }
        } catch (BadPaddingException exception) {
            throw new InvalidSignature();
        }

        // Verify that the block is valid and contains correct previous block hash
        if (tail != null && block.getPreviousBlockHash() != null && !block.getPreviousBlockHash().equals(tail.getHash())) {
            throw new InvalidHashException();
        }

        // Check that the input block hasn't been already spent
        BlockchainIterator it = (BlockchainIterator) iterator();

        Block currentBlock;
        if (block.getInputBlock() != null) {
            while (it.hasNext()) {
                currentBlock = (Block) it.next();

                // A block that used the specified input block has already been stored in the chain
                if (currentBlock.getInputBlock() != null && currentBlock.getInputBlock().getHash().equals(block.getInputBlock().getHash())) {
                    throw new DoubleSpendingException();
                }
            }
        }

        // Accept the block as the tail
        tail = block;

    }

    /**
     * Iterator over all block contained in the blockchain, starting from the last block
     * @return iterator over all blocks
     */
    public Iterator<Block> iterator() {
        return new BlockchainIterator(tail);
    }

}
