package FoodChain.blockchain;

import FoodChain.entity.Entity;

import javax.money.MonetaryAmount;

/**
 * Used to represent money transfers, private constructor ensures that money used for transactions can't appear out of thin air
 */
public class MoneyTransfer {

    private Entity from;
    private MonetaryAmount amount;

    /**
     * Creates a MoneyTransfer object and correctly subtracts money from the wallet
     * @param wallet wallet from which the money is sent
     * @param amount amount of money to be transferred
     * @return MoneyTransfer object containing the information about the transfer
     */
    static MoneyTransfer createTransfer(Wallet wallet, double amount) {
        MonetaryAmount monetaryAmount = wallet.withdrawMoney(amount);
        return new MoneyTransfer(wallet.getOwner(), monetaryAmount);
    }

    private MoneyTransfer(Entity from, MonetaryAmount amount) {
        this.from = from;
        this.amount = amount;
    }

    /**
     * Returns the transferred amount of money
     * @return amount of sent money
     */
    public MonetaryAmount getAmount() {
        return amount;
    }

    /**
     * Returns the entity that sent the money
     * @return money sender
     */
    public Entity getFrom() {
        return from;
    }
}
