package FoodChain;

import FoodChain.entity.Entity;
import FoodChain.foodStuff.FoodState;
import FoodChain.foodStuff.FoodStuff;
import FoodChain.foodStuff.FoodType;
import FoodChain.foodStuff.StateType;

/**
 * Representation of a request with information about the request and it's progress.
 */
public class Request {
    private Entity from;
    private Entity to;
    private int price;
    protected boolean done;
    protected boolean accepted;
    protected RequestType type;
    private StateType foodState;
    private FoodType foodType;

    /**
     * Creates a request with given parameters.
     * @param from - Entity issuing request
     * @param type - DEMAND/OFFER
     * @param state - State of foodstuff offered/demanded
     * @param foodType - Type of foodstuff offered/demanded
     * @param price - Price of transaction
     */
    public Request(Entity from, RequestType type, StateType state, FoodType foodType, int price) {
        this.from = from;
        this.type = type;
        this.foodState = state;
        this.foodType = foodType;
        this.price = price;
        this.done = false;
    }

    /**
     * Methods for entities to accept a request if still possible.
     * @param to - Entity wanting to accept the request
     * @return True if request assigned to entity asking for it, false otherwise
     */
    public boolean accept(Entity to) {
        if (accepted) return false;
        this.to = to;
        accepted = true;
        return true;
    }

    /**
     * Tells whether request has been completely dealt with or not.
     * @return True if request done and finished, false otherwise
     */
    public boolean isDone() {
        return done;
    }

    /**
     * Sets request to completely done and finished.
     */
    public void setDone() {
        this.done = true;
    }

    /**
     * Tells which entity has issued the request.
     * @return Entity by which the request has been issued
     */
    public Entity getFrom() {
        return from;
    }

    /**
     * Tells which entity accepted the request.
     * @return Entity that accepted the request
     */
    public Entity getTo() {
        return to;
    }

    /**
     * Tells food type requested/offered.
     * @return Food type requested/offered
     */
    public FoodType getFoodType() {
        return foodType;
    }

    /**
     * Tells food state type requested/offered.
     * @return Food state type requested/offered
     */
    public StateType getStateType() {
        return foodState;
    }

    /**
     * Tells price point request has been issued with.
     * @return Price of foodstuff offered/demanded
     */
    public int getPrice() {
        return price;
    }

    /**
     * Tells whether request has been accepted yet or not.
     * @return True if request has been accepted, false otherwise
     */
    public boolean isAccepted() {
        return accepted;
    }

    /**
     * Tells type of request.
     * @return Type of request (DEMAND/OFFER]
     */
    public RequestType getType() {
        return type;
    }
}
