package FoodChain;

/**
 * Interface for iterable collections.
 * @param <E> - Class to iterate over.
 */
public interface Iterable<E> {
    Iterator<E> iterator();
}
