package Simulation;

import FoodChain.RequestType;
import FoodChain.blockchain.BlockchainDistributor;
import FoodChain.channel.Channel;
import FoodChain.channel.ChannelFactory;
import FoodChain.entity.Entity;
import FoodChain.entity.EntityFactory;
import FoodChain.entity.ProductiveVegFarmer;
import FoodChain.foodStuff.StateType;

import java.util.Random;

/**
 * Basic simulation configuration, there is no entity that would be processing every type of food
 */
public class BasicSimulation extends Simulation {

    private ChannelFactory channelFactory;
    private EntityFactory entityFactory;

    private Channel grapeDemand;
    private Channel grapeOffer;
    private Channel wheatDemand;
    private Channel wheatOffer;
    private Channel meatDemand;
    private Channel meatOffer;
    private Channel tomatoDemand;
    private Channel tomatoOffer;
    private Channel potatoDemand;
    private Channel potatoOffer;

    private Entity farmer1;
    private Entity farmer2;
    private Entity farmer3;
    private Entity processor1;
    private Entity processor2;
    private Entity storage1;
    private Entity storage2;
    private Entity distributor1;
    private Entity distributor2;
    private Entity seller1;
    private Entity seller2;
    private Entity customer1;
    private Entity customer2;
    private Entity customer3;

    @Override
    protected void createEntities() {
        Random random = new Random();
        entityFactory = new EntityFactory(channelFactory.getFirstChannel());
        farmer1 = entityFactory.makeMeatFarmer(random.nextInt(800) + 200);
        farmer2 = entityFactory.makeVegFarmer(random.nextInt(800) + 200);
        farmer3 = entityFactory.makeVegFarmer(random.nextInt(800) + 200);
        processor1 = entityFactory.makeProcessor(random.nextInt(800) + 200, StateType.FARMED);
        processor2 = entityFactory.makeProcessor(random.nextInt(800) + 200, StateType.RAW);
        storage1 = entityFactory.makeStorage(random.nextInt(800) + 200);
        storage2 = entityFactory.makeStorage(random.nextInt(800) + 200);
        distributor1 = entityFactory.makeDistributor(random.nextInt(800) + 200);
        distributor2 = entityFactory.makeDistributor(random.nextInt(800) + 200);
        seller1 = entityFactory.makeSeller(random.nextInt(800) + 200);
        seller2 = entityFactory.makeSeller(random.nextInt(800) + 200);
        customer1 = entityFactory.makeCustomer(random.nextInt(800) + 200);
        customer2 = entityFactory.makeCustomer(random.nextInt(800) + 200);
        customer3 = entityFactory.makeCustomer(random.nextInt(800) + 200);

        entities.add(farmer1);
        entities.add(farmer2);
        entities.add(farmer3);
        entities.add(processor1);
        entities.add(processor2);
        entities.add(storage1);
        entities.add(storage2);
        entities.add(distributor1);
        entities.add(distributor2);
        entities.add(seller1);
        entities.add(seller2);
        entities.add(customer1);
        entities.add(customer2);
        entities.add(customer3);

        meatDemand.attachListener(farmer1);
        wheatDemand.attachListener(farmer2);
        grapeDemand.attachListener(farmer2);
        tomatoDemand.attachListener(farmer2);
        potatoDemand.attachListener(farmer2);
        wheatDemand.attachListener(farmer3);
        grapeDemand.attachListener(farmer3);
        tomatoDemand.attachListener(farmer3);
        potatoDemand.attachListener(farmer3);

        meatOffer.attachListener(processor1);
        meatDemand.attachListener(processor1);
        meatOffer.attachListener(storage1);
        meatDemand.attachListener(storage1);
        meatOffer.attachListener(distributor1);
        meatDemand.attachListener(distributor1);
        meatOffer.attachListener(seller1);
        meatDemand.attachListener(seller1);
        meatOffer.attachListener(customer1);

        wheatOffer.attachListener(processor1);
        wheatDemand.attachListener(processor1);
        wheatOffer.attachListener(storage1);
        wheatDemand.attachListener(storage1);
        wheatOffer.attachListener(distributor1);
        wheatDemand.attachListener(distributor1);
        wheatOffer.attachListener(seller1);
        wheatDemand.attachListener(seller1);
        wheatOffer.attachListener(customer1);

        grapeOffer.attachListener(processor2);
        grapeDemand.attachListener(processor2);
        grapeOffer.attachListener(storage2);
        grapeDemand.attachListener(storage2);
        grapeOffer.attachListener(distributor2);
        grapeDemand.attachListener(distributor2);
        grapeOffer.attachListener(seller2);
        grapeDemand.attachListener(seller2);
        grapeOffer.attachListener(customer2);

        tomatoOffer.attachListener(processor2);
        tomatoDemand.attachListener(processor2);
        tomatoOffer.attachListener(storage2);
        tomatoDemand.attachListener(storage2);
        tomatoOffer.attachListener(distributor2);
        tomatoDemand.attachListener(distributor2);
        tomatoOffer.attachListener(seller2);
        tomatoDemand.attachListener(seller2);
        tomatoOffer.attachListener(customer2);

        potatoOffer.attachListener(processor2);
        potatoDemand.attachListener(processor2);
        potatoOffer.attachListener(storage2);
        potatoDemand.attachListener(storage2);
        potatoOffer.attachListener(distributor2);
        potatoDemand.attachListener(distributor2);
        potatoOffer.attachListener(seller2);
        potatoDemand.attachListener(seller2);
        potatoOffer.attachListener(customer2);

        wheatOffer.attachListener(customer3);
        grapeOffer.attachListener(customer3);
        potatoOffer.attachListener(customer3);
        tomatoOffer.attachListener(customer3);
    }

    @Override
    protected void createChannels() {
        channelFactory = new ChannelFactory();

        grapeDemand = channelFactory.makeGrapeChannel(RequestType.DEMAND);
        grapeOffer = channelFactory.makeGrapeChannel(RequestType.OFFER);
        wheatDemand = channelFactory.makeWheatChannel(RequestType.DEMAND);
        wheatOffer = channelFactory.makeWheatChannel(RequestType.OFFER);
        tomatoDemand = channelFactory.makeTomatoChannel(RequestType.DEMAND);
        tomatoOffer = channelFactory.makeTomatoChannel(RequestType.OFFER);
        potatoDemand = channelFactory.makePotatoChannel(RequestType.DEMAND);
        potatoOffer = channelFactory.makePotatoChannel(RequestType.OFFER);
        meatDemand = channelFactory.makeMeatChannel(RequestType.DEMAND);
        meatOffer = channelFactory.makeMeatChannel(RequestType.OFFER);

    }

}
