package Simulation;

import FoodChain.RequestType;
import FoodChain.channel.Channel;
import FoodChain.channel.ChannelFactory;
import FoodChain.entity.Entity;
import FoodChain.entity.EntityFactory;
import FoodChain.foodStuff.StateType;

import java.util.Random;

/**
 * Simulation with less farmers and customers (end points) and more processors. Some entities trying to deal all
 * foodstuff types, are dedicated to particular types.
 */
public class NotSoBasicSimulation extends Simulation {
    private ChannelFactory channelFactory;
    private EntityFactory entityFactory;

    private Channel grapeDemand;
    private Channel grapeOffer;
    private Channel wheatDemand;
    private Channel wheatOffer;
    private Channel meatDemand;
    private Channel meatOffer;
    private Channel tomatoDemand;
    private Channel tomatoOffer;
    private Channel potatoDemand;
    private Channel potatoOffer;

    private Entity farmer1;
    private Entity farmer2;

    private Entity processor1;
    private Entity processor2;
    private Entity processor3;
    private Entity processor4;

    private Entity storage1;
    private Entity storage2;
    private Entity storage3;

    private Entity distributor1;
    private Entity distributor2;
    private Entity distributor3;

    private Entity seller1;
    private Entity seller2;
    private Entity seller3;

    private Entity customer1;


    @Override
    protected void createEntities() {
        Random random = new Random();
        entityFactory = new EntityFactory(channelFactory.getFirstChannel());
        farmer1 = entityFactory.makeMeatFarmer(random.nextInt(2000) + 200);
        farmer2 = entityFactory.makeVegFarmer(random.nextInt(2000) + 200);
        processor1 = entityFactory.makeProcessor(random.nextInt(800) + 10000, StateType.FARMED);
        processor2 = entityFactory.makeProcessor(random.nextInt(800) + 5000, StateType.RAW);
        processor3 = entityFactory.makeProcessor(random.nextInt(800) + 200, StateType.FARMED);
        processor4 = entityFactory.makeProcessor(random.nextInt(800) + 1500, StateType.RAW);
        storage1 = entityFactory.makeStorage(random.nextInt(800) + 600);
        storage2 = entityFactory.makeStorage(random.nextInt(800) + 1000);
        storage3 = entityFactory.makeStorage(random.nextInt(800) + 5000);
        distributor1 = entityFactory.makeDistributor(random.nextInt(800) + 2000);
        distributor2 = entityFactory.makeDistributor(random.nextInt(800) + 3000);
        distributor3 = entityFactory.makeDistributor(random.nextInt(800) + 40000);
        seller1 = entityFactory.makeSeller(random.nextInt(800) + 600);
        seller2 = entityFactory.makeSeller(random.nextInt(800) + 15000);
        seller3 = entityFactory.makeSeller(random.nextInt(800) + 4000);
        customer1 = entityFactory.makeCustomer(random.nextInt(800) + 5523);

        entities.add(farmer1);
        entities.add(farmer2);
        entities.add(processor1);
        entities.add(processor2);
        entities.add(processor3);
        entities.add(processor4);
        entities.add(storage1);
        entities.add(storage2);
        entities.add(storage3);
        entities.add(distributor1);
        entities.add(distributor2);
        entities.add(distributor3);
        entities.add(seller1);
        entities.add(seller2);
        entities.add(seller3);
        entities.add(customer1);

        meatDemand.attachListener(farmer1);
        wheatDemand.attachListener(farmer2);
        grapeDemand.attachListener(farmer2);
        tomatoDemand.attachListener(farmer2);
        potatoDemand.attachListener(farmer2);

        wheatOffer.attachListener(processor1);
        grapeOffer.attachListener(processor1);
        tomatoOffer.attachListener(processor1);
        potatoOffer.attachListener(processor1);

        meatOffer.attachListener(processor2);

        tomatoOffer.attachListener(processor3);
        potatoOffer.attachListener(processor3);

        wheatOffer.attachListener(processor4);
        grapeOffer.attachListener(processor4);

        wheatDemand.attachListener(processor1);
        grapeDemand.attachListener(processor1);
        tomatoDemand.attachListener(processor1);
        potatoDemand.attachListener(processor1);

        meatDemand.attachListener(processor2);

        tomatoDemand.attachListener(processor3);
        potatoDemand.attachListener(processor3);

        wheatDemand.attachListener(processor4);
        grapeDemand.attachListener(processor4);

        meatDemand.attachListener(storage1);
        wheatDemand.attachListener(storage1);
        grapeDemand.attachListener(storage1);
        tomatoDemand.attachListener(storage1);
        potatoDemand.attachListener(storage1);

        meatDemand.attachListener(storage2);
        wheatDemand.attachListener(storage2);
        grapeDemand.attachListener(storage3);
        tomatoDemand.attachListener(storage3);
        potatoDemand.attachListener(storage3);

        meatOffer.attachListener(storage1);
        wheatOffer.attachListener(storage1);
        grapeOffer.attachListener(storage1);
        tomatoOffer.attachListener(storage1);
        potatoOffer.attachListener(storage1);

        meatOffer.attachListener(storage2);
        wheatOffer.attachListener(storage2);
        grapeOffer.attachListener(storage3);
        tomatoOffer.attachListener(storage3);
        potatoOffer.attachListener(storage3);



        meatDemand.attachListener(distributor1);
        wheatDemand.attachListener(distributor1);
        grapeDemand.attachListener(distributor1);
        tomatoDemand.attachListener(distributor1);
        potatoDemand.attachListener(distributor1);

        meatDemand.attachListener(distributor2);
        wheatDemand.attachListener(distributor2);
        grapeDemand.attachListener(distributor3);
        tomatoDemand.attachListener(distributor3);
        potatoDemand.attachListener(distributor3);

        meatOffer.attachListener(distributor1);
        wheatOffer.attachListener(distributor1);
        grapeOffer.attachListener(distributor1);
        tomatoOffer.attachListener(distributor1);
        potatoOffer.attachListener(distributor1);

        meatOffer.attachListener(distributor2);
        wheatOffer.attachListener(distributor2);
        grapeOffer.attachListener(distributor3);
        tomatoOffer.attachListener(distributor3);
        potatoOffer.attachListener(distributor3);



        meatDemand.attachListener(seller1);
        wheatDemand.attachListener(seller1);
        grapeDemand.attachListener(seller1);
        tomatoDemand.attachListener(seller1);
        potatoDemand.attachListener(seller1);

        meatDemand.attachListener(seller2);
        wheatDemand.attachListener(seller2);
        grapeDemand.attachListener(seller3);
        tomatoDemand.attachListener(seller3);
        potatoDemand.attachListener(seller3);

        meatOffer.attachListener(seller1);
        wheatOffer.attachListener(seller1);
        grapeOffer.attachListener(seller1);
        tomatoOffer.attachListener(seller1);
        potatoOffer.attachListener(seller1);

        meatOffer.attachListener(seller2);
        wheatOffer.attachListener(seller2);
        grapeOffer.attachListener(seller3);
        tomatoOffer.attachListener(seller3);
        potatoOffer.attachListener(seller3);


        meatOffer.attachListener(customer1);
        wheatOffer.attachListener(customer1);
        grapeOffer.attachListener(customer1);
        potatoOffer.attachListener(customer1);
        tomatoOffer.attachListener(customer1);
    }

    @Override
    protected void createChannels() {
        channelFactory = new ChannelFactory();

        grapeDemand = channelFactory.makeGrapeChannel(RequestType.DEMAND);
        grapeOffer = channelFactory.makeGrapeChannel(RequestType.OFFER);
        wheatDemand = channelFactory.makeWheatChannel(RequestType.DEMAND);
        wheatOffer = channelFactory.makeWheatChannel(RequestType.OFFER);
        tomatoDemand = channelFactory.makeTomatoChannel(RequestType.DEMAND);
        tomatoOffer = channelFactory.makeTomatoChannel(RequestType.OFFER);
        potatoDemand = channelFactory.makePotatoChannel(RequestType.DEMAND);
        potatoOffer = channelFactory.makePotatoChannel(RequestType.OFFER);
        meatDemand = channelFactory.makeMeatChannel(RequestType.DEMAND);
        meatOffer = channelFactory.makeMeatChannel(RequestType.OFFER);
        System.out.println("Channels created");
        System.out.println(meatDemand == null);

    }

}
