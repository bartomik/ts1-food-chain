package processTests;

import FoodChain.RequestType;
import FoodChain.blockchain.Block;
import FoodChain.blockchain.Blockchain;
import FoodChain.blockchain.BlockchainDistributor;
import FoodChain.blockchain.DoubleSpendingException;
import FoodChain.channel.Channel;
import FoodChain.channel.ChannelFactory;
import FoodChain.entity.Entity;
import FoodChain.foodStuff.FoodStuff;
import FoodChain.foodStuff.FoodType;
import FoodChain.foodStuff.Meat;
import FoodChain.foodStuff.Wheat;
import FoodChain.operations.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class mischiefProcessTests {
    private Entity farmer;
    private Channel channel;
    private FoodStuff food;

    @BeforeEach
    public void setup() {
        ChannelFactory channelFactory = new ChannelFactory();

        channel = channelFactory.makeGrapeChannel(RequestType.DEMAND);
        farmer = new Entity(channel, 1000, null);

        food = new Meat();
        Operation meatObtained = new GatherOperation(farmer, farmer, food);
        farmer.getBlockchainClient().storeOperation(meatObtained, null, 0);
    }

    @Test
    public void testDetectsDoubleSpending() {
        // arrange
        FoodType type = FoodType.MEAT;
        List<Map.Entry<FoodStuff, Block>> list = farmer.getBlockchainClient().getWallet().getAllFoodOfType(type);
        Block inputBlock = list.get(0).getValue();
        Entity buyer1 = new Entity(channel, 500, null);
        Entity buyer2 = new Entity(channel, 500, null);

        // act
        Operation firstTransfer = new TransferOperation(farmer, buyer1, food, 10);
        Operation secondTransfer = new TransferOperation(farmer, buyer2, food, 20);
        farmer.getBlockchainClient().storeOperation(firstTransfer, inputBlock, 0);
        farmer.getBlockchainClient().storeOperation(secondTransfer, inputBlock, 1);

        // assert
        Map<Entity, Integer> map = BlockchainDistributor.getInstance().getDoubleSpendingAttempts();
        Integer attempts = map.get(farmer);
        Integer expectedAttempts = 1;
        assertEquals(expectedAttempts, attempts);
    }

    @Test
    public void testDetectsChangesToBlocks() {
        // arrange
        FoodType type = FoodType.MEAT;
        List<Map.Entry<FoodStuff, Block>> list = farmer.getBlockchainClient().getWallet().getAllFoodOfType(type);
        Block inputBlock = list.get(0).getValue();
        Entity buyer1 = new Entity(channel, 500, null);
        Operation transfer = new TransferOperation(farmer, buyer1, food, 10);

        // act
        farmer.getBlockchainClient().storeOperation(transfer, inputBlock, 0);
        Block transferBlock = buyer1.getBlockchainClient().getWallet().getAllFoodOfType(type).get(0).getValue();
        inputBlock.setSignature("123".getBytes(StandardCharsets.UTF_8));

        // assert
        assertFalse(farmer.getBlockchainClient().getBlockchain().verify());
        assertTrue(buyer1.getBlockchainClient().getBlockchain().verify());

        // act
        transferBlock.setOperation(new CleanOperation(buyer1, buyer1, food));

        // assert
        assertFalse(buyer1.getBlockchainClient().getBlockchain().verify());
    }
}


