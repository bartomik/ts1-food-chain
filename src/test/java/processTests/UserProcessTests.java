package processTests;

import FoodChain.RequestType;
import FoodChain.blockchain.Block;
import FoodChain.channel.Channel;
import FoodChain.channel.ChannelFactory;
import FoodChain.entity.Entity;
import FoodChain.foodStuff.*;
import FoodChain.operations.FinishOperation;
import FoodChain.operations.GatherOperation;
import FoodChain.operations.Operation;
import FoodChain.operations.TransferOperation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.*;
import java.util.*;


import static org.junit.jupiter.api.Assertions.*;

public class UserProcessTests {
    private Entity farmer;
    private Channel channel;

    @BeforeEach
    public void setup() {
        ChannelFactory channelFactory = new ChannelFactory();

        channel = channelFactory.makeGrapeChannel(RequestType.DEMAND);
        farmer = new Entity(channel, 1000, null);
    }

    @Test
    public void farmAndStoreFood() {
        // arrange
        FoodStuff food = new Meat();
        FoodType meat = FoodType.MEAT;

        // act
        Operation meatObtained = new GatherOperation(farmer, farmer, food);
        farmer.getBlockchainClient().storeOperation(meatObtained, null, 0);

        // assert
        List<Map.Entry<FoodStuff, Block>> list = farmer.getBlockchainClient().getWallet().getAllFoodOfType(meat);
        int expectedNumOfMeat = 1;
        assertFalse(list.isEmpty());
        assertEquals(expectedNumOfMeat, list.size());
    }

    @Test
    public void farmAndSendFood() {
        // arrange
        FoodStuff newFood = new Meat();
        Operation meatObtained = new GatherOperation(farmer, farmer, newFood);
        FoodType meat = FoodType.MEAT;
        Entity buyer = new Entity(channel, 500, null);
        Operation transfer = new TransferOperation(farmer, buyer, newFood, 30);

        // act
        farmer.getBlockchainClient().storeOperation(meatObtained, null, 0);
        List<Map.Entry<FoodStuff, Block>> list = farmer.getBlockchainClient().getWallet().getAllFoodOfType(meat);
        farmer.getBlockchainClient().storeOperation(transfer, list.get(0).getValue(), 1);
        List<Map.Entry<FoodStuff, Block>> farmerList = farmer.getBlockchainClient().getWallet().getAllFoodOfType(meat);
        List<Map.Entry<FoodStuff, Block>> buyerList = buyer.getBlockchainClient().getWallet().getAllFoodOfType(meat);


        // assert
        int expectedNumOfMeat = 1;
        int expectedMoney = 470;
        assertAll("Test wallets of both parties",
                () -> assertFalse(farmerList.isEmpty()),
                () -> assertFalse(buyerList.isEmpty()),
                () -> assertEquals(expectedNumOfMeat, buyerList.size()),
                () -> assertEquals(expectedMoney, buyer.getBlockchainClient().
                        getWallet().getMoneyBalance()));
    }

    @Test
    public void ProcessFood() {
        FoodStuff meatFood = new Meat();

        StateType resultStateFirst = meatFood.getFoodState().getType();
        meatFood.process();

        StateType resultStateSecond = meatFood.getFoodState().getType();
        meatFood.process();

        StateType resultStateThird = meatFood.getFoodState().getType();
        meatFood.process();

        StateType resultStateFourth = meatFood.getFoodState().getType();
        meatFood.process();

        StateType resultStateFifth = meatFood.getFoodState().getType();

        Operation meatFinish = new FinishOperation(farmer, farmer, meatFood);

        FoodType meat = FoodType.MEAT;

        assertAll("state order test",
                () -> assertEquals(StateType.RAW, resultStateFirst),
                () -> assertEquals(StateType.CLEANED, resultStateSecond),
                () -> assertEquals(StateType.PACKAGED, resultStateThird),
                () -> assertEquals(StateType.CERTIFIED, resultStateFourth),
                () -> assertEquals(StateType.FINAL, resultStateFifth));
    }

    @Test
    public void processAndSendFood() {
        // arrange
        Entity partner = new Entity(channel, 500, null);
        FoodStuff meatFood = new Meat();
        FoodType meat = FoodType.MEAT;

        // act
        meatFood.process();
        StateType resultStateAfterFarmer = meatFood.getFoodState().getType();
        Operation meatObtained = new GatherOperation(farmer, farmer, meatFood);
        Operation transfer = new TransferOperation(farmer, partner, meatFood, 0);
        Operation transferBack = new TransferOperation(partner, farmer, meatFood, 30);
        farmer.getBlockchainClient().storeOperation(meatObtained, null, 0);
        List<Map.Entry<FoodStuff, Block>> list = farmer.getBlockchainClient().
                getWallet().getAllFoodOfType(meat);
        farmer.getBlockchainClient().storeOperation(transfer, null, 0);
        List<Map.Entry<FoodStuff, Block>> listBuyer = partner.getBlockchainClient().
                getWallet().getAllFoodOfType(meat);
        StateType resultOfMeat = listBuyer.get(0).getKey().process().getFoodState().getType();
        partner.getBlockchainClient().storeOperation(transferBack, null, 0);

        // assert
        double resultMoneyAfterTransfer = partner.getBlockchainClient().
                getWallet().getMoneyBalance();
        double expMoneyAfterTransfer = 530;
        StateType exp = farmer.getBlockchainClient().getWallet().getAllFoodOfType(meat).get(0).
                getKey().getFoodState().getType();
        StateType expTypeFirst = StateType.CLEANED;
        StateType expTypeSecond = StateType.PACKAGED;
        assertAll("State order test",
                () -> assertEquals(expTypeFirst, resultStateAfterFarmer),
                () -> assertEquals(expTypeSecond, resultOfMeat),
                () -> assertEquals(expMoneyAfterTransfer, resultMoneyAfterTransfer),
                () -> assertEquals(expTypeSecond, exp));
    }

    @Test
    public void sendFoodAndBackMoney() {
        // arrange
        Entity buyer = new Entity(channel, 500, null);
        FoodStuff meatFood = new Meat();
        meatFood.process();

        // act
        Operation meatObtained = new GatherOperation(farmer, farmer, meatFood);
        Operation transfer = new TransferOperation(farmer, buyer, meatFood, 50);
        farmer.getBlockchainClient().storeOperation(meatObtained, null, 0);
        farmer.getBlockchainClient().storeOperation(transfer, null, 0);
        double moneyBeforeReturn = buyer.getBlockchainClient().getWallet().getMoneyBalance();
        farmer.getBlockchainClient().getWallet().transferMoneyTo(buyer, 50);

        // assert
        double expMoney = buyer.getBlockchainClient().getWallet().getMoneyBalance();
        double moneyAfterReturn = buyer.getBlockchainClient().getWallet().getMoneyBalance();
        double expMoneyBefore = 450;
        double expMoneyAfter = 500;
        assertEquals(expMoneyBefore, moneyBeforeReturn);
        assertEquals(expMoneyAfter, moneyAfterReturn);

    }

    @Test
    public void processSendAndCleanOperations() {
        // arrange
        Entity buyer = new Entity(channel, 500, null);
        FoodStuff meatFood = new Meat();
        FoodStuff meatChicken = new Meat();
        FoodStuff grapeFood = new Grape();
        meatFood.process();
        grapeFood.process();
        meatFood.process();
        grapeFood.process();

        // act
        Operation meatObtained = new GatherOperation(farmer, farmer, meatFood);
        Operation grapeObtained = new GatherOperation(farmer, farmer, grapeFood);
        Operation transferMeat = new TransferOperation(farmer, buyer, meatFood, 50);
        Operation transferGrape = new TransferOperation(farmer, buyer, grapeFood, 50);
        farmer.getBlockchainClient().storeOperation(meatObtained, null, 0);
        farmer.getBlockchainClient().storeOperation(grapeObtained, null, 0);
        farmer.getBlockchainClient().storeOperation(transferGrape, null, 0);
        farmer.getBlockchainClient().storeOperation(transferMeat, null, 0);

        List<Map.Entry<FoodStuff, Block>> buyerMeat = buyer.getBlockchainClient().getWallet().getAllFoodOfType(FoodType.MEAT);
        List<Map.Entry<FoodStuff, Block>> buyerGrape = buyer.getBlockchainClient().getWallet().getAllFoodOfType(FoodType.GRAPE);
        int expNumOfFood = 1;
        double buyerMoney = buyer.getBlockchainClient().getWallet().getMoneyBalance();
        double farmerMoney = farmer.getBlockchainClient().getWallet().getMoneyBalance();
        double expMoneyOfFarmer = 1100;
        double expMoneyOfBuyer = 400;

        farmer.getBlockchainClient().cleanPerformedOperations();
        List<Operation> numOfOperations = farmer.getBlockchainClient().getPerformedOperations();
        int expNumOfOperations = 0;

        // assert
        assertAll("Several food test",
                () -> assertEquals(expNumOfFood, buyerMeat.size()),
                () -> assertEquals(expNumOfFood, buyerGrape.size()),
                () -> assertEquals(expMoneyOfFarmer, farmerMoney),
                () -> assertEquals(expMoneyOfBuyer, buyerMoney),
                () -> assertEquals(expNumOfOperations, numOfOperations.size()));
    }
}
