package FoodChain.blockchain;

import FoodChain.entity.Entity;
import FoodChain.operations.Operation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.security.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


/**
 * Class containing various tests with respect to Blockchain assuring that the code is valid
 */
@ExtendWith(MockitoExtension.class)
public class BlockchainTest {
    @Mock
    Operation mockOperation;

    @Mock
    Entity mockFarmer;

        private PrivateKey privateKey;
    private PublicKey publicKey;


    private Block genesisBlock;
    private Blockchain blockchain;


    @BeforeEach
    public void setup() {

        // Create the key pair
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            KeyPair pair = keyPairGenerator.generateKeyPair();
            privateKey = pair.getPrivate();
            publicKey = pair.getPublic();
        } catch (NoSuchAlgorithmException exception) {
            System.out.println(exception.getMessage());
            System.exit(1);
        }

        genesisBlock = new Block(null, null, null, mockOperation, 0, null);

        when(mockOperation.getFrom()).thenReturn(mockFarmer);
        when(mockFarmer.getPublicKey()).thenReturn(publicKey);

        try {
            genesisBlock.sign(privateKey);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        blockchain = new Blockchain(genesisBlock);
    }

    @Test
    public void appendBlock_correctBlock_appendsBlock() {
        // arrange
        Block nextBlock = new Block(blockchain.getLastBlock(), blockchain.getLastBlock().getHash(), genesisBlock, mockOperation, 2);
        try {
            nextBlock.sign(privateKey);
            blockchain.appendBlock(nextBlock);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        assertEquals(nextBlock, blockchain.getLastBlock());
    }

    // unit test
    @Test
    public void appendBlock_invalidHash_exceptionThrown() {
        // arrange
        Block nextBlock = new Block(blockchain.getLastBlock(), "wrong hash", genesisBlock, mockOperation, 2);
        try {
            nextBlock.sign(privateKey);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


        // act + assert
        assertThrows(InvalidHashException.class, () -> blockchain.appendBlock(nextBlock));
    }


    @Test
    void appendBlock_doubleSpending_exceptionThrown() {
        // arrange
        Block nextBlock = new Block(blockchain.getLastBlock(), blockchain.getLastBlock().getHash(), genesisBlock, mockOperation, 2);
        try {
            nextBlock.sign(privateKey);
            blockchain.appendBlock(nextBlock);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        Block doubleSpendingBlock = new Block(blockchain.getLastBlock(), blockchain.getLastBlock().getHash(), genesisBlock, mockOperation, 3);
        try {
            doubleSpendingBlock.sign(privateKey);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // act + assert
        assertThrows(DoubleSpendingException.class, () -> {
            blockchain.appendBlock(doubleSpendingBlock);
        });

    }


    // unit test
    @Test
    public void verify_correctBlockchain_True() {
        // arrange
        Block secondBlock = new Block(blockchain.getLastBlock(), blockchain.getLastBlock().getHash(), genesisBlock, mockOperation, 2);
        try {
            secondBlock.sign(privateKey);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        Block thirdBlock = new Block(secondBlock, secondBlock.getHash(), secondBlock, mockOperation, 3);
        try {
            thirdBlock.sign(privateKey);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // act
        try {
            blockchain.appendBlock(secondBlock);
            blockchain.appendBlock(thirdBlock);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // assert
        assertTrue(blockchain.verify());
    }
}
