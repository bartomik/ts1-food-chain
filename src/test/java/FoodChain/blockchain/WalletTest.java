package FoodChain.blockchain;

import FoodChain.entity.Entity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.stream.Stream;

import static org.mockito.Mockito.verify;

/**
 * Class containing various tests with respect to Wallet assuring that the code is valid
 */
@ExtendWith(MockitoExtension.class)
public class WalletTest {

    @Mock
    Entity entity;

    Wallet myWallet;

    @BeforeEach
    public void setup() {
        myWallet = new Wallet(entity, 1000);
    }


    @ParameterizedTest(name = "{0} to be withdrawn - {1} should be withdrawn, {2} should remain in wallet")
    @MethodSource("provideParametersForWithdraw")
    public void withdrawMoney_parameterizedTest_validResult(double amount, double expectedResult, double expectedAccountState) {
        // act
        double result = myWallet.withdrawMoney(amount).getNumber().doubleValue();
        double resultAccountState = myWallet.getMoneyBalance();

        // assert
        Assertions.assertAll("Should return correct withdrawn amount and correct account state after operation",
                () -> Assertions.assertEquals(expectedResult, result),
                () -> Assertions.assertEquals(expectedAccountState, resultAccountState));
    }

    @ParameterizedTest(name = "{0} to be transfered - {1} should remain in wallet")
    @MethodSource("provideParametersForTransaction")
    public void transferMoneyTo_parameterizedTest_validResult(double amount, double expected) {
        myWallet.transferMoneyTo(entity, amount);
        double resultAccountState = myWallet.getMoneyBalance();

        verify(entity).receiveMoney(ArgumentMatchers.any(MoneyTransfer.class));
        Assertions.assertEquals(resultAccountState, expected);
    }

    private static Stream<Arguments> provideParametersForWithdraw() {
        return Stream.of(
                Arguments.of(995, 995, 5),
                Arguments.of(999, 999, 1),
                Arguments.of(999.5, 999.5, 0.5),
                Arguments.of(999.99, 999.99, 0.01),
                Arguments.of(1000, 1000, 0),
                Arguments.of(1000.0001, 0, 1000),
                Arguments.of(1000.5, 0, 1000),
                Arguments.of(1001, 0, 1000),
                Arguments.of(1005, 0, 1000)
        );
    }

    private static Stream<Arguments> provideParametersForTransaction() {
        return Stream.of(
                Arguments.of(995, 5),
                Arguments.of(999, 1),
                Arguments.of(999.5, 0.5),
                Arguments.of(999.99, 0.01),
                Arguments.of(1000, 0),
                Arguments.of(1000.0001, 1000),
                Arguments.of(1000.5, 1000),
                Arguments.of(1001, 1000),
                Arguments.of(1005, 1000)
        );
    }

}
