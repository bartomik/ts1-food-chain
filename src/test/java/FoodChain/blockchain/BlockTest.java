package FoodChain.blockchain;

import FoodChain.operations.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.crypto.BadPaddingException;
import java.security.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class containing various tests with respect to Block assuring that the code is valid
 */
@ExtendWith(MockitoExtension.class)
public class BlockTest {

    @Mock
    Operation mockOperation;

    private PrivateKey privateKey;
    private PublicKey publicKey;
    private Block genesisBlock;


    @BeforeEach
    public void setup() {

        // Create the key pair
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            KeyPair pair = keyPairGenerator.generateKeyPair();
            privateKey = pair.getPrivate();
            publicKey = pair.getPublic();
        } catch (NoSuchAlgorithmException exception) {
            System.out.println(exception.getMessage());
            System.exit(1);
        }

        genesisBlock = new Block(null, null, null, mockOperation,0, null);

        try {
            genesisBlock.sign(privateKey);
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }


    }

    @Test
    public void sign_correctPrivateKey_True() {
        // arrange
        Block otherBlock = new Block(genesisBlock, genesisBlock.getHash(), genesisBlock, mockOperation, 1);


        // act + assert
        Assertions.assertDoesNotThrow(() -> otherBlock.sign(privateKey));
        Assertions.assertTrue(otherBlock.getSignature().length > 0);
    }

    @Test
    public void sign_wrongPrivateKey_False() {
        // arrange
        Block otherBlock = new Block(genesisBlock, genesisBlock.getHash(), genesisBlock, mockOperation, 1);
        PrivateKey flawedPrivateKey = new PrivateKey() {
            @Override
            public String getAlgorithm() {
                return null;
            }

            @Override
            public String getFormat() {
                return null;
            }

            @Override
            public byte[] getEncoded() {
                return new byte[0];
            }
        };

        // act + assert
        assertThrows(InvalidSignature.class, () -> otherBlock.sign(flawedPrivateKey));
    }


    @Test
    public void clone_validBlock_blockClonedCorrectly() {
        // arrange
        Block otherBlock = new Block(genesisBlock, genesisBlock.getHash(), genesisBlock, mockOperation, 1);
        try {
            otherBlock.sign(privateKey);
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }

        // act
        Block nextBlockClone = (Block) otherBlock.clone();

        // assert
        assertNotNull(nextBlockClone);
        Assertions.assertAll("Should return properly cloned block",
                () -> assertEquals(otherBlock.getHash(), nextBlockClone.getHash()),
                () -> assertEquals(otherBlock.getPreviousBlock(), nextBlockClone.getPreviousBlock()),
                () -> assertEquals(otherBlock.getInputBlock(), nextBlockClone.getInputBlock()),
                () -> assertEquals(otherBlock.getOperation(), nextBlockClone.getOperation()),
                () -> assertEquals(otherBlock.getTimestamp(), nextBlockClone.getTimestamp()),
                () -> Assertions.assertArrayEquals(otherBlock.getSignature(), nextBlockClone.getSignature()));
    }

    @Test
    public void verifySignature_correctPublicKey_True() throws BadPaddingException {
        Assertions.assertTrue(genesisBlock.verifySignature(publicKey));
    }

    @Test
    public void verifySignature_foreignPublicKey_False() throws BadPaddingException {
        // arrange
        PublicKey foreignPublicKey = null;

        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            KeyPair pair = keyPairGenerator.generateKeyPair();
            foreignPublicKey = pair.getPublic();
        } catch (NoSuchAlgorithmException exception) {
            System.out.println(exception.getMessage());
            System.exit(1);
        }

        // act + assert
        Assertions.assertFalse(genesisBlock.verifySignature(foreignPublicKey));
    }
}
