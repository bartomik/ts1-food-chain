# Food Chain 
## Description
Project implements a simple blockchain over which a system for following the processing stages of food from is realized.
Entities such as farmers, distributors, storage centers, sellers and customers trade food items through designated channels that they can subscribe to.
All transactions are stored on a blockchain platform that verifies and stores all transactions along with import information about the transactions.

## Parts of application
### Blockchain
The blockhain itself is realized in several classes such sa Blockchain, Block or Wallet.

### Entities
These represent the different people involved in the food chain. Entities communicate with the blockchain platform through their own blockhain client.

### Channels
Channels are used to transfer food items from one entity to another. They allow entities to request or demand certaing food items in food-specific channels.

### Simulation
Example of how the system might run. Scenarios are created manually, entities are given strategies how to behave on the market.

Link to software testing documentation: https://docs.google.com/document/d/18TWpLA4_vt_KKkTz8CKCyeXZsHNYG0fSnZT96Vx_2rk/edit?usp=sharing

(Also can be found in this repository in PDF format)

